package com.lz.common;

public class ArrayUtils{

    public  static  int [] INT_ARRAY_12 =new int[]{7,34,67,1,4,5,3,6,3,89,13,54};

    public  static String toString(int [] a) {
        if (a == null)
            return "null";
        int iMax = a.length - 1;
        if (iMax == -1)
            return "[]";
        return toString(a, new StringBuilder()).toString();
    }

    public static  StringBuilder toString(int [] a, StringBuilder b) {

        int iMax = a.length - 1;

        b.append('[');
        for (int i = 0; ; i++) {
            b.append(String.valueOf(a[i]));
            if (i == iMax)
                return b.append(']');
            b.append(", ");
        }
    }

    public static String toString2(int [][] a) {
        if (a == null)
            return "null";

        int iMax = a.length - 1;
        if (iMax == -1)
            return "[]";

        StringBuilder b = new StringBuilder();
        b.append('[');
        b.append('\n');
        for (int i = 0; ; i++) {

            toString(a[i], b);
            if (i == iMax)
                return b.append("\n]").toString();
            b.append("\n");
        }
    }


}
