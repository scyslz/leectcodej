package com.lz.stack;

import java.util.Stack;

/**
 * 简易计算器 带有括号优先级
 */
public class Calculator {

    public static void main(String[] args) {
        Calculator calculator = new Calculator();
        calculator.simpleCalculator("5+9-12");
        calculator.simpleCalculator01("5+9/3-3*4/2");
//        int i = calculator.help("10+(2*16-20/5)+7*2");
//        int i = calculator.help02("(((-3+2)*2)-3)/(-3)");
        int i = calculator.simpleCalculator03("10+(2*16-20/5)+7*2");
        System.out.println(i);
    }

    /**
     * 加、减 计算器
     * 5+9-1
     *
     * @param s
     * @return
     */
    int simpleCalculator(String s) {
        int length = s.length();
        int num = 0;
        Stack<Integer> stack = new Stack<>();
        char asign = '+';
        for (int i = 0; i < length; i++) {
            char ch = s.charAt(i);

            boolean digit = Character.isDigit(ch);

            if (digit) {
                int n = ch - '0';
                num = 10 * num + n;
            }
            if (!digit || i == length - 1) {
                switch (asign) {
                    case '+':
                        stack.push(num);
                        break;
                    case '-':
                        stack.push(-num);
                        break;
                }
                num = 0;
                asign = ch;
            }
        }
        int sum = 0;
        while (!stack.isEmpty()) {
            sum += stack.pop();
        }
        return sum;
    }

    /**
     * 支持乘除
     *
     * @param s
     * @return
     */
    int simpleCalculator01(String s) {
        int length = s.length();
        int num = 0;
        Stack<Integer> stack = new Stack<>();
        char asign = '+';
        for (int i = 0; i < length; i++) {
            char ch = s.charAt(i);

            boolean digit = Character.isDigit(ch);

            if (digit) {
                int n = ch - '0';
                num = 10 * num + n;
            }
            if (!digit || i == length - 1) {
                switch (asign) {
                    case '*':
                        Integer pop = stack.pop();
                        stack.push(pop * num);
                        break;
                    case '/':
                        Integer divde = stack.pop();
                        stack.push(divde / num);
                        break;
                    case '+':
                        stack.push(num);
                        break;
                    case '-':
                        stack.push(-num);
                        break;
                }
                num = 0;
                asign = ch;
            }
        }
        int sum = 0;
        while (!stack.isEmpty()) {
            sum += stack.pop();
        }
        return sum;

    }

    /**
     * 带有括号优先级
     *
     * @param s
     * @return
     */

    int i = 0;

    int simpleCalculator02(String s) {
        int length = s.length();
        int num = 0;
        Stack<Integer> stack = new Stack<>();
        char asign = '+';
        for (; i < length; i++) {
            char ch = s.charAt(i);


            boolean digit = Character.isDigit(ch);
            if (digit) {
                int n = ch - '0';
                num = 10 * num + n;
            }
            if (ch == '(') {
                i++;
                num = simpleCalculator02(s);
            }
            if (!digit || i == length - 1) {
                switch (asign) {
                    case '*':
                        Integer pop = stack.pop();
                        stack.push(pop * num);
                        break;
                    case '/':
                        Integer divde = stack.pop();
                        stack.push(divde / num);
                        break;
                    case '+':
                        stack.push(num);
                        break;
                    case '-':
                        stack.push(-num);
                        break;
                }
                num = 0;
                if (ch == ')') {
                    break;
                }
                asign = ch;
            }


        }
        int sum = 0;
        while (!stack.isEmpty()) {
            sum += stack.pop();
        }
        return sum;
    }

    Stack<Integer> stack = new Stack<>();

    /**
     * 带有优先级
     * @param s
     * @return
     */
    int simpleCalculator03(String s) {
        int length = s.length();
        int num = 0;
        char asign = '+';
        int j = 0;
        for (; i < length; i++) {
            char ch = s.charAt(i);

            boolean digit = Character.isDigit(ch);
            if (digit) {
                int n = ch - '0';
                num = 10 * num + n;
            }
            if (ch == '(') {
                i++;
                num = simpleCalculator03(s);
            }
            if (!digit || i == length - 1) {
                j++;
                switch (asign) {
                    case '*':
                        Integer pop = stack.pop();
                        stack.push(pop * num);
                        break;
                    case '/':
                        Integer divde = stack.pop();
                        stack.push(divde / num);
                        break;
                    case '+':
                        stack.push(num);
                        break;
                    case '-':
                        stack.push(-num);
                        break;
                }
                num = 0;
                if (ch == ')') {
                    break;
                }
                asign = ch;
            }

        }
        int sum = 0;
        while (j-- > 0 && stack.size()>0) {
            sum += stack.pop();
        }
        return sum;
    }

}
