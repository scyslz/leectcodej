package com.lz.linknode;

public class Leetcode {

    /**
     * 合并两个链表 ，递归实现
     *
     * 1。终止条件
     * 2。递归
     * 3。处理哦
     * @param l1
     * @param l2
     * @return
     */
    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        // ListNode list=new ListNode(0);
        // ListNode x=list;
        // while(l1!=null || l2!=null){
        //     if(l1==null) {list.next=l2; l2=l2.next;}
        //     else if(l2==null){ list.next=l1; l1=l1.next;}
        //     else if(l1.val<l2.val) { list.next=l1; l1=l1.next;}
        //     else  {list.next=l2; l2=l2.next;}

        //     list=list.next;
        // }
        // return x.next;

        if (l1 == null) return l2;
        if (l2 == null) return l1;
        if (l1.val < l2.val) {
            l1.next = mergeTwoLists(l1.next, l2);
            return l1;
        }
        l2.next = mergeTwoLists(l1, l2.next);
        return l2;
    }

}
